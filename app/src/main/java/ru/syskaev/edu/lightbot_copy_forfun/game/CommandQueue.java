package ru.syskaev.edu.lightbot_copy_forfun.game;

import java.util.ArrayList;

public class CommandQueue extends ArrayList<Game.Command> {

    public static final int MAX_SIZE = 32;

    private boolean launchFlag = false;
    private int startPosition = 0;
    String msg;

    public void setLaunchFlag(boolean launchFlag) { this.launchFlag = launchFlag; }

    public synchronized String getMsg() {
        String tempMsg = msg;
        msg = null;
        return tempMsg;
    }

    @Override
    public boolean add(Game.Command command) {
        if(size() == MAX_SIZE) {
            msg = "Commands limit!";
            return false;
        } else if(launchFlag) {
            msg = "Game already launch!";
            return false;
        }
        return super.add(command);
    }

    public boolean canPop() {
        return startPosition < size();
    }

    public Game.Command pop() {
        Game.Command tempCommand = get(startPosition);
        set(startPosition++, Game.Command.VOID);
        return tempCommand;
    }

    @Override
    public void clear() {
        super.clear();
        launchFlag = false;
        startPosition = 0;
    }
}
