package ru.syskaev.edu.lightbot_copy_forfun.game;

public class Game {

    private static final long LAUNCH_THREAD_SLEEP_TIME = 750;

    public enum Command {VOID, FORWARD, LIGHT, RIGHT, LEFT, JUMP}

    private CommandQueue commands;
    private GameLevel gameLevel;
    private Bot bot;
    String msg;

    public Game() {
        commands = new CommandQueue();
        gameLevel = new GameLevel();
        gameLevel.generate();
        bot = new Bot(0,0,0, gameLevel);
    }

    public CommandQueue getCommands() {
        return commands;
    }
    public synchronized GameLevel getGameLevel() { return gameLevel; }
    public synchronized Bot getBot() { return bot; }

    public synchronized String getMsg() {
        if (msg == null)
            msg = bot.getMsg();
        if (msg == null)
            msg = commands.getMsg();
        String tempMsg = msg;
        msg = null;
        return tempMsg;
    }

    public void launch() {
        commands.setLaunchFlag(true);
        (new LaunchThread()).start();
    }

    class LaunchThread extends Thread {
        @Override
        public void run() {
            while(commands.canPop()) {
                switch(commands.pop()) {
                    case FORWARD:
                        bot.doForwardCommand();
                        break;
                    case LIGHT:
                        bot.doLightCommand();
                        break;
                    case RIGHT:
                        bot.doRightCommand();
                        break;
                    case LEFT:
                        bot.doLeftCommand();
                        break;
                    case JUMP:
                        bot.doJumpCommand();
                        break;
                }
                try {
                    Thread.sleep(LAUNCH_THREAD_SLEEP_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(gameLevel.getTargetCounter() == 0)
                msg = "You win!!!";
            else {
                msg = "Game Over! Restart.";
                restart();
            }
        }
    }

    public void restart() {
        commands.clear();
        gameLevel.generate();
        bot.restart();
    }

}
