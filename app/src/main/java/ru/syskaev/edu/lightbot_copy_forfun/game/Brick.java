package ru.syskaev.edu.lightbot_copy_forfun.game;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import ru.syskaev.edu.lightbot_copy_forfun.R;

public class Brick {

    private static Bitmap brick_white, brick_white_b, brick_white_y, brick_light_gray,
            brick_light_gray_b, brick_light_gray_y, brick_dark_gray;

    private Boolean exist;
    private Boolean target = false;
    private Boolean checked = false;
    private int x, y, z;

    public Brick(Boolean exist, int x, int y, int z) {
        this.exist = exist;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Boolean isExist() { return exist; }
    public Boolean isTarget() { return target; }
    public Boolean isChecked() { return checked; }

    public int getX() { return x; }
    public int getY() { return y; }
    public int getZ() { return z; }
    public void setExist(Boolean exist) { this.exist = exist; }
    public void setTarget(Boolean target) { this.target = target; }
    public void setChecked(Boolean checked) { this.checked = checked; }

    public static void prepareBitmaps(Resources res) {
        brick_white = BitmapFactory.decodeResource(res, R.drawable.brick_white);
        brick_white_b = BitmapFactory.decodeResource(res, R.drawable.brick_white_b);
        brick_white_y = BitmapFactory.decodeResource(res, R.drawable.brick_white_y);
        brick_light_gray = BitmapFactory.decodeResource(res, R.drawable.brick_light_gray);
        brick_light_gray_b = BitmapFactory.decodeResource(res, R.drawable.brick_light_gray_b);
        brick_light_gray_y = BitmapFactory.decodeResource(res, R.drawable.brick_light_gray_y);
        brick_dark_gray = BitmapFactory.decodeResource(res, R.drawable.brick_dark_gray);
    }

    public synchronized Bitmap getBitmap() {
        if(z == 0) {
            if(checked)
                return brick_white_y;
            else if(target)
                return brick_white_b;
            else
                return brick_white;
        }
        else if(z == 1) {
            if(checked)
                return brick_light_gray_y;
            else if(target)
                return brick_light_gray_b;
            else
                return brick_light_gray;
        }
        else return  brick_dark_gray;
    }
}
