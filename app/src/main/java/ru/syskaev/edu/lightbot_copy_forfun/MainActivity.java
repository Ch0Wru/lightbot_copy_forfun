package ru.syskaev.edu.lightbot_copy_forfun;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import ru.syskaev.edu.lightbot_copy_forfun.game.Game;

public class MainActivity extends AppCompatActivity {

    private static final long DRAW_THREAD_SLEEP_TIME = 50;

    private Game game;
    private CommandView commandView;
    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        game = new Game();
        setViews();
        setClickListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        (new DrawThread()).start();
    }

    private void setViews() {
        ViewGroup layout = findViewById(R.id.command_panel);
        commandView = new CommandView(this, game.getCommands());
        commandView.setLayoutParams(new FrameLayout.LayoutParams
                (FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        layout.addView(commandView);
        layout = findViewById(R.id.game_panel);
        gameView = new GameView(this, game);
        gameView.setLayoutParams(new FrameLayout.LayoutParams
                (FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        layout.addView(gameView);
    }

    private void setClickListeners() {
        findViewById(R.id.button_launch).setOnClickListener(v -> game.launch());
        findViewById(R.id.button_restart).setOnClickListener(v -> game.restart());
        findViewById(R.id.button_forward).
                setOnClickListener(v -> game.getCommands().add(Game.Command.FORWARD));
        findViewById(R.id.button_light).
                setOnClickListener(v -> game.getCommands().add(Game.Command.LIGHT));
        findViewById(R.id.button_left).
                setOnClickListener(v -> game.getCommands().add(Game.Command.LEFT));
        findViewById(R.id.button_right).
                setOnClickListener(v -> game.getCommands().add(Game.Command.RIGHT));
        findViewById(R.id.button_jump).
                setOnClickListener(v -> game.getCommands().add(Game.Command.JUMP));
    }

    class DrawThread extends Thread {
        @Override
        public void run() {
            while(true) {
                try {
                    Thread.sleep(DRAW_THREAD_SLEEP_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                commandView.invalidate();
                gameView.invalidate();
            }
        }
    }

}
