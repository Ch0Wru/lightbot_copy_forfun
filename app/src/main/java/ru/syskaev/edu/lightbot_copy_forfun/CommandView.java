package ru.syskaev.edu.lightbot_copy_forfun;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import ru.syskaev.edu.lightbot_copy_forfun.game.CommandQueue;

public class CommandView extends View {

    private static final int DEFAULT_VIEW_WIDTH = 570;
    private static final int ICONS_LINEAR_SIZE = 50;
    private static final int ICONS_HORIZONTAL_NUMBER = 8;
    private static final int ICONS_MARGIN = 10;

    Paint paint;
    Bitmap forward, light, right, left, jump;
    CommandQueue commands;

    public CommandView(Context context, CommandQueue commands) {
        super(context);
        this.commands = commands;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        prepareBitmaps();
    }

    private void prepareBitmaps() {
        forward = BitmapFactory.decodeResource(getResources(), R.drawable.forward);
        light = BitmapFactory.decodeResource(getResources(), R.drawable.light);
        right = BitmapFactory.decodeResource(getResources(), R.drawable.right);
        left = BitmapFactory.decodeResource(getResources(), R.drawable.right);
        jump = BitmapFactory.decodeResource(getResources(), R.drawable.jump);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int size = commands.size();
        for(int i = 0; i != size; i++) {
            Bitmap tempBitmap = forward;
            switch(commands.get(i)) {
                case VOID:
                    continue;
                case FORWARD:
                    break;
                case LIGHT:
                    tempBitmap = light;
                    break;
                case RIGHT:
                    tempBitmap = right;
                    break;
                case LEFT:
                    tempBitmap = left;
                    break;
                case JUMP:
                    tempBitmap = jump;
                    break;
            }
            canvas.drawBitmap(tempBitmap, null, calcRectF(i), paint);
        }
    }

    private Rect calcRectF(int pos) {
        int left = ICONS_MARGIN +
                (DEFAULT_VIEW_WIDTH / ICONS_HORIZONTAL_NUMBER) * (pos % ICONS_HORIZONTAL_NUMBER);
        int top = ICONS_MARGIN +
                (DEFAULT_VIEW_WIDTH / ICONS_HORIZONTAL_NUMBER) * (pos / ICONS_HORIZONTAL_NUMBER);
        return new Rect(
                left, top, left + ICONS_LINEAR_SIZE, top + ICONS_LINEAR_SIZE);
    }

}
