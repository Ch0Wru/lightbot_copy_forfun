package ru.syskaev.edu.lightbot_copy_forfun.game;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import ru.syskaev.edu.lightbot_copy_forfun.R;

public class Bot {

    private static Bitmap toX, toY, tonX, tonY;

    private static final int X = 0;
    private static final int Y = 1;
    private static final int nX = 2;
    private static final int nY = 3;
    private static final int[] NEXT_RIGHT = {1, 2, 3, 0};
    private static final int[] NEXT_LEFT = {3, 0, 1, 2};
    private static final int[] NEXT_X_DELTA = {1, 0, -1, 0};
    private static final int[] NEXT_Y_DELTA = {0, 1, 0, -1};

    private int x, y, z, direction;
    GameLevel gameLevel;
    String msg;

    public Bot(int x, int y, int z, GameLevel gameLevel) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.gameLevel = gameLevel;
        direction = 0;
    }

    public synchronized int getX() { return x; }
    public synchronized int getY() { return y; }
    public synchronized int getZ() { return z; }

    public synchronized String getMsg() {
        String tempMsg = msg;
        msg = null;
        return tempMsg;
    }

    public static void prepareBitmaps(Resources res) {
        toX = BitmapFactory.decodeResource(res, R.drawable.arrow_x);
        toY = BitmapFactory.decodeResource(res, R.drawable.arrow_y);
        tonX = BitmapFactory.decodeResource(res, R.drawable.arrow_nx);
        tonY = BitmapFactory.decodeResource(res, R.drawable.arrow_ny);
    }

    public synchronized Bitmap getBitmap() {
        if(direction == X) return toX;
        else if(direction == Y) return toY;
        else if(direction == nX) return tonX;
        else return tonY;
    }

    public void doForwardCommand() {
        int newx = x + NEXT_X_DELTA[direction];
        int newy = y + NEXT_Y_DELTA[direction];
        if(gameLevel.withinBounds(newx, newy, z) && gameLevel.getBrick(newx, newy, z).isExist() &&
                (!gameLevel.withinBounds(newx, newy, z + 1)
                        || !gameLevel.getBrick(newx, newy, z + 1).isExist())) {
            x = newx;
            y = newy;
        } else
            msg = "Can't forward!";
    }

    public void doJumpCommand() {
        int newx = x + NEXT_X_DELTA[direction];
        int newy = y + NEXT_Y_DELTA[direction];
        if(gameLevel.withinBounds(newx, newy, z + 1) && gameLevel.getBrick(newx, newy, z + 1).isExist() &&
                (!gameLevel.withinBounds(newx, newy, z + 2)
                        || !gameLevel.getBrick(newx, newy, z + 2).isExist())) {
            x = newx;
            y = newy;
            z++;
        } else if(gameLevel.withinBounds(newx, newy, z - 1) && gameLevel.getBrick(newx, newy, z - 1).isExist() &&
                (!gameLevel.withinBounds(newx, newy, z)
                        || !gameLevel.getBrick(newx, newy, z).isExist())) {
            x = newx;
            y = newy;
            z--;
        } else
            msg = "Can't jump!";
    }

    public void doLightCommand() {
        Brick tempBrick = gameLevel.getBrick(x, y, z);
        if(tempBrick.isTarget() && !tempBrick.isChecked()) {
            tempBrick.setChecked(true);
            gameLevel.decrementTargetCounter();
        }
        else
            msg = "Can't light!";
    }

    public void doRightCommand() {
        direction = NEXT_RIGHT[direction];
    }

    public void doLeftCommand() {
        direction = NEXT_LEFT[direction];
    }

    public void restart() {
        x = y = z = 0;
        direction = 0;
    }

}
