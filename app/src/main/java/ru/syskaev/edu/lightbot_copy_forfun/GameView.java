package ru.syskaev.edu.lightbot_copy_forfun;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import ru.syskaev.edu.lightbot_copy_forfun.game.Bot;
import ru.syskaev.edu.lightbot_copy_forfun.game.Brick;
import ru.syskaev.edu.lightbot_copy_forfun.game.Game;

public class GameView extends View {

    private static final int DEFAULT_VIEW_WIDTH = 720;

    private static final int ZERO_X = 55;
    private static final int ZERO_Y = 250;
    private static final int DELTA_X_COMPONENT_X = 50;
    private static final int DELTA_X_COMPONENT_Y = -16;
    private static final int DELTA_Y_COMPONENT_X = 50;
    private static final int DELTA_Y_COMPONENT_Y = 16;
    private static final int DELTA_Z_COMPONENT_Y = -50;

    private static final int BRICK_WIDTH = 100;
    private static final int BRICK_HEIGHT = 80;
    private static final int BOT_WIDTH = 55;
    private static final int BOT_HEIGHT = 43;

    private static final int BOT_DELTA_X = 18;
    private static final int BOT_DELTA_Y = -10;

    private static final int TEXT_ZERO_Y = 600;
    private static final int DEFAULT_SHOW_TEXT_COUNTER = 15;

    Paint paint, fontPaint;
    Game game;
    Bot bot;
    String msg;
    int showTextCounter = 0;

    public GameView(Context context, Game game) {
        super(context);
        this.game = game;
        bot = game.getBot();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        prepareBitmaps();

        fontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fontPaint.setTextSize(40);
        fontPaint.setStyle(Paint.Style.STROKE);
    }

    private void prepareBitmaps() {
        Brick.prepareBitmaps(getResources());
        Bot.prepareBitmaps(getResources());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for(Brick brick : game.getGameLevel().getBricksArray())
            if(brick.isExist()) {
                canvas.drawBitmap(brick.getBitmap(), null, calcRectForBrick(brick), paint);
                if(bot.getX() == brick.getX() && bot.getY() == brick.getY()
                        && bot.getZ() == brick.getZ())
                    canvas.drawBitmap(bot.getBitmap(), null, calcRectForBot(), paint);
            }
        if(showTextCounter > 0)
            drawText(canvas, msg);
        else {
            msg = game.getMsg();
            if(msg != null) {
                showTextCounter = DEFAULT_SHOW_TEXT_COUNTER;
                drawText(canvas, msg);
            }
        }
    }

    private Rect calcRectForBrick(Brick brick) {
        int left = ZERO_X + brick.getX() * DELTA_X_COMPONENT_X + brick.getY() * DELTA_Y_COMPONENT_X;
        int top = ZERO_Y + brick.getX() * DELTA_X_COMPONENT_Y + brick.getY() * DELTA_Y_COMPONENT_Y
                + brick.getZ() * DELTA_Z_COMPONENT_Y;
        return new Rect(
                left, top, left + BRICK_WIDTH, top + BRICK_HEIGHT);
    }

    private Rect calcRectForBot() {
        int left = ZERO_X + bot.getX() * DELTA_X_COMPONENT_X + bot.getY() * DELTA_Y_COMPONENT_X
                + BOT_DELTA_X;
        int top = ZERO_Y + bot.getX() * DELTA_X_COMPONENT_Y + bot.getY() * DELTA_Y_COMPONENT_Y
                + BOT_DELTA_Y + bot.getZ() * DELTA_Z_COMPONENT_Y;
        return new Rect(
                left, top, left + BOT_WIDTH, top + BOT_HEIGHT);
    }

    private void drawText(Canvas canvas, String msg) {
        showTextCounter--;
        float width = fontPaint.measureText(msg);
        float[] widths = new float[msg.length()];
        fontPaint.getTextWidths(msg, widths);
        canvas.drawText(msg, (DEFAULT_VIEW_WIDTH - width) / 2, TEXT_ZERO_Y, fontPaint);

    }

}
