package ru.syskaev.edu.lightbot_copy_forfun.game;

import java.util.ArrayList;
import java.util.Random;

public class GameLevel {

    private static final int LEVEL_LINEAR_SIZE = 6;
    private static final int LEVEL_HEIGHT = 3;
    private static final int MAX_OF_TARGET = 3;
    private static final double PROBABILITY_OF_SECOND_LAYER = 0.25;
    private static final double PROBABILITY_OF_THIRD_LAYER = 0.15;
    private static final double PROBABILITY_OF_TARGET = 0.05;

    private Brick[][][] bricks = new Brick[LEVEL_LINEAR_SIZE][LEVEL_LINEAR_SIZE][LEVEL_HEIGHT];
    private ArrayList<Brick> bricksArray =
            new ArrayList<>(LEVEL_LINEAR_SIZE * LEVEL_LINEAR_SIZE * LEVEL_HEIGHT);
    private Random random = new Random();
    private int targetCounter;

    public GameLevel() { }

    public synchronized ArrayList<Brick> getBricksArray() { return bricksArray; }
    public synchronized Brick getBrick(int x, int y, int z) { return bricks[x][y][z]; }
    public synchronized int getTargetCounter() { return targetCounter; }

    public synchronized boolean withinBounds(int x, int y, int z) {
        return x >= 0 && y >= 0 && z >= 0
                && x < LEVEL_LINEAR_SIZE && y < LEVEL_LINEAR_SIZE && z < LEVEL_HEIGHT;
    }

    public synchronized void decrementTargetCounter() {
        targetCounter--;
    }

    public void generate() {
        bricksArray.clear();
        targetCounter = 0;
        for(int z = 0; z != LEVEL_HEIGHT; z++)
            for(int x = LEVEL_LINEAR_SIZE - 1; x >= 0; x--)
                for(int y = 0; y != LEVEL_LINEAR_SIZE; y++) {
                    Brick brick = new Brick(false, x, y, z);
                    if(z == 0)
                        brick.setExist(true);
                    else if(z == 1 && random.nextDouble() < PROBABILITY_OF_SECOND_LAYER
                            && (x != 0 || y != 0))
                        brick.setExist(true);
                    else if(z == 2 && getBrick(x, y, 1).isExist()
                            && random.nextDouble() < PROBABILITY_OF_THIRD_LAYER)
                        brick.setExist(true);
                    bricks[x][y][z] = brick;
                    bricksArray.add(brick);
                }
        generateTargetBricks();
    }

    private void generateTargetBricks() {
        for(int z = 0; z != LEVEL_HEIGHT; z++)
            for(int x = 0; x != LEVEL_LINEAR_SIZE; x++)
                for(int y = 0; y != LEVEL_LINEAR_SIZE; y++) {
                    if((z == 0 && !getBrick(x, y,1).isExist()
                            || z == 1 && getBrick(x, y, z).isExist() && !getBrick(x, y,2).isExist())
                            && random.nextDouble() < PROBABILITY_OF_TARGET) {
                        bricks[x][y][z].setTarget(true);
                        if (++targetCounter == MAX_OF_TARGET)
                            return;
                    }
                }

    }

}
